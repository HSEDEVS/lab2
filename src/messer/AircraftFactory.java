package messer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.*;


import senser.AircraftSentence;

/* Get the following datafields out of the JSON sentence using Regex and String methods
 * and return a BasicAircraft
 * 
 * For Lab3 replace usee JSO parsing instead
 * 
 * "Icao":"3C5467", matches; first part is Icao, second part is 3C5467
 * "Op":"Lufthansa matches; first part is Op, second part is Lufthansa
 * "Species":1, matches; first part is Species, second part is 1
 * "PosTime":1504179914003, matches; first part is PosTime, second part is 1504179914003
 * "Lat":49.1912, matches; first part is Lat, second part is 49.1912
 * "Long":9.3915, matches; first part is Long, second part is 9.3915
 * "Spd":420.7, matches; first part is Spd, second part is 420.7
 * "Trak":6.72, matches; first part is Trak, second part is 6.72
 * "GAlt":34135, matches; first part is GAlt, second part is 34135
 */

//Regex StackOverflow: https://stackoverflow.com/questions/1454913/regular-expression-to-find-a-string-included-between-two-characters-while-exclud
public class AircraftFactory {

	public BasicAircraft fromAircraftSentence ( AircraftSentence sentence ) {
		String icao = null;
		String operator = null;
		int species = 0;
		Date posTime = null;
		double longitude = 0;
		double latitude = 0;
		double speed = 0;
		double trak = 0;
		int altitude = 0;

		// TODO: Your code goes here
		Matcher matcher;
		
		//ICAO
		Pattern icao_exp = Pattern.compile("(?<=\\\"Icao\\\":\\\")(.*?)(?=\\\")");
		matcher = icao_exp.matcher(sentence.getAircraftData());
		
		if(matcher.find())
			icao = matcher.group();
		
		//OPERATOR
		Pattern operator_exp = Pattern.compile("(?<=\\\"Op\\\":\\\")(.*?)(?=\\\")");
		matcher = operator_exp.matcher(sentence.getAircraftData());
		
		if(matcher.find())
			operator = matcher.group();
		
		//SPECIES
		Pattern species_exp = Pattern.compile("(?<=\"Species\":)(.*?)(?=,)");
		matcher = species_exp.matcher(sentence.getAircraftData());
		
		if(matcher.find())
			species = Integer.parseInt(matcher.group());
		
		//POSTIME
		Pattern posTime_exp = Pattern.compile("(?<=\"PosTime\":)(.*?)(?=,)");
		matcher = posTime_exp.matcher(sentence.getAircraftData());
		
		if(matcher.find()) {
			//https://stackoverflow.com/questions/535004/unix-epoch-time-to-java-date-object
			Date posTime_buff = new Date(Long.parseLong(matcher.group()));
			posTime = posTime_buff;
		}
		
		//LATITUDE
		Pattern latitude_exp = Pattern.compile("(?<=\"Lat\":)(.*?)(?=,)");
		matcher = latitude_exp.matcher(sentence.getAircraftData());
		
		if(matcher.find()) 
			latitude = Double.parseDouble(matcher.group());
		

		//LONGITUDE
		Pattern longitude_exp = Pattern.compile("(?<=\"Long\":)(.*?)(?=,)");
		matcher = longitude_exp.matcher(sentence.getAircraftData());
		
		if(matcher.find()) 
			longitude = Double.parseDouble(matcher.group());
		
		//SPEED
		Pattern speed_exp = Pattern.compile("(?<=\"Spd\":)(.*?)(?=,)");
		matcher = speed_exp.matcher(sentence.getAircraftData());
		
		if(matcher.find())
			speed = Double.parseDouble(matcher.group());
		
		//TRAK
		Pattern trak_exp = Pattern.compile("(?<=\"Trak\":)(.*?)(?=,)");
		matcher = trak_exp.matcher(sentence.getAircraftData());
		
		if(matcher.find())
			trak = Double.parseDouble(matcher.group());
		
		//ALTITUDE
		Pattern altitude_exp = Pattern.compile("(?<=\"GAlt\":)(.*?)(?=,)");
		matcher = altitude_exp.matcher(sentence.getAircraftData());
		
		if(matcher.find())
			altitude = Integer.parseInt(matcher.group());
		
			
			
		BasicAircraft msg = new BasicAircraft(icao, operator, species, posTime, new Coordinate(latitude, longitude), speed, trak, altitude);
		
		return msg;
	}
}
